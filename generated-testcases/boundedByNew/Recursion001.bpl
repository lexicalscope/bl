type Ref;
type Heap;
type Data = [Ref]Ref;
type RefSet = [Ref]bool;
const unique null:Ref;

function HeapCons(Data,RefSet,RefSet):Heap;
function Data(Heap):Data;
function Allocated(Heap):RefSet;
function Roots(Heap):RefSet;

axiom (forall d:Data, al:RefSet, rts:RefSet :: {HeapCons(d,al,rts)} Data(HeapCons(d,al,rts))==d);
axiom (forall d:Data, al:RefSet, rts:RefSet :: {HeapCons(d,al,rts)} Allocated(HeapCons(d,al,rts))==al);
axiom (forall d:Data, al:RefSet, rts:RefSet :: {HeapCons(d,al,rts)} Roots(HeapCons(d,al,rts))==rts);

function {:inline true} WriteField(h:Heap, a:Ref, v:Ref) : Heap {HeapCons(Data(h)[a:=v], Allocated(h), Roots(h))}
function {:inline true} ReadField(h:Heap, a:Ref) : Ref {Data(h)[a]}
function {:inline true} Edge(h:Heap,a1:Ref,a2:Ref) : bool {ReadField(h,a1)==a2}
function {:inline true} Allocate(h:Heap, a:Ref) : Heap {HeapCons(Data(h), Allocated(h)[a:=true], Roots(h))}
function {:inline true} IsAllocated(h:Heap, a:Ref) : bool {Allocated(h)[a]}
function {:inline true} IsRoot(h:Heap, a:Ref) : bool {Roots(h)[a]}

///////////////////////////////////////////////////////////////////
// All the heaps created by BL programs have certain properties
function $GoodHeap(h:Heap) : bool;
axiom (forall h:Heap,a:Ref :: $GoodHeap(h) ==> !IsAllocated(h,a) ==> Edge(h,a,null));  
axiom (forall h:Heap :: $GoodHeap(h) ==> IsAllocated(h,null));
axiom (forall h:Heap :: $GoodHeap(h) ==> Edge(h,null,null));
axiom (forall h:Heap,a1,a2:Ref :: $GoodHeap(h) ==> IsAllocated(h,a1) && Edge(h,a1,a2) ==> IsAllocated(h,a2));
axiom (forall h:Heap,a:Ref :: {$ModOne(h,a)} $GoodHeap(h) ==> IsRoot(h,a) ==> IsAllocated(h,a));

///////////////////////////////////////////////////////////////////
// Used for performance, we only want to instantiate some
// quantifiers with objects explicitly allocated by the 
// body of a procedure
function $NewOne(h:Heap,a:Ref) : bool;
function $ModOne(h:Heap,a:Ref) : bool;

///////////////////////////////////////////////////////////////////
// HeapSuccessor
function $HeapSucc(h1,h2:Heap) : bool;
axiom (forall h:Heap,a,v:Ref :: {$GoodHeap(WriteField(h,a,v))} $GoodHeap(WriteField(h,a,v)) ==> $HeapSucc(h,WriteField(h,a,v)));
axiom (forall h:Heap,a:Ref :: {$GoodHeap(Allocate(h,a))} $GoodHeap(Allocate(h,a)) ==> $HeapSucc(h,Allocate(h,a)));
axiom (forall h1,h2,h3:Heap :: {$HeapSucc(h1,h2), $HeapSucc(h2,h3)} $HeapSucc(h1,h2) && $HeapSucc(h2,h3) ==> $HeapSucc(h1,h3));
axiom (forall h1,h2:Heap,a:Ref :: {$HeapSucc(h1,h2),$ModOne(h1,a)} $HeapSucc(h1,h2) && $ModOne(h1,a) ==> $ModOne(h2,a));
axiom (forall h1,h2:Heap,a:Ref :: {$HeapSucc(h1,h2),$NewOne(h1,a)} $HeapSucc(h1,h2) && $NewOne(h1,a) ==> $NewOne(h2,a));

///////////////////////////////////////////////////////////////////
// for now we do not allow free, but we only need to avoid
// address 1) memory reuse 2) read/write of freed memory
// which we could do by tracking freed memory and checking
// before each memory access
function NoFrees(h1,h2:Heap) : bool
{ (forall a:Ref :: IsAllocated(h1,a) ==> IsAllocated(h2,a))}

///////////////////////////////////////////////////////////////////
// Extensionality for Heaps
function Heap#Equal(Heap, Heap): bool;
axiom (forall h1,h2: Heap :: 
 Heap#Equal(h1,h2) <==> (forall a1,a2: Ref :: 
  {$ModOne(h1,a1),IsAllocated(h1,a2)} 
  {$ModOne(h2,a1),IsAllocated(h2,a2)} 
  (Edge(h1,a1,a2) == Edge(h2,a1,a2))) && 
  (forall a:Ref :: {$NewOne(h1,a)} {$NewOne(h2,a)} IsAllocated(h1,a)==IsAllocated(h2,a)));

axiom (forall h1,h2: Heap :: Heap#Equal(h1,h2) ==> h1 == h2);

///////////////////////////////////////////////////////////////////
// Roots are all allocated
function Roots#Equal(Heap, Heap): bool;
axiom (forall h1,h2:Heap :: Roots#Equal(h1,h2) <==> (forall a:Ref :: IsRoot(h1,a)<==>IsRoot(h2,a)));

///////////////////////////////////////////////////////////////////
// Same Modifications for Heaps
function Heap#SameMods(Heap, Heap, Heap, Heap): bool;
axiom (forall h1,h2,h3,h4:Heap ::
	Heap#SameMods(h1,h3,h2,h4) <==> 
	(forall a: Ref :: {$ModOne(h1,a)} {$NewOne(h1,a)} {IsAllocated(h1,a)} {IsAllocated(h2,a)} {IsAllocated(h3,a)} {IsAllocated(h4,a)} 
    	(ReadField(h2,a)!=ReadField(h4,a) ==> ReadField(h3,a)==ReadField(h4,a)) &&
    	(ReadField(h1,a)!=ReadField(h3,a) ==> ReadField(h3,a)==ReadField(h4,a)) &&
    	(!IsAllocated(h1,a) && IsAllocated(h3,a) <==> !IsAllocated(h2,a) && IsAllocated(h4,a)) &&
    	(IsAllocated(h1,a) && !IsAllocated(h3,a) <==> IsAllocated(h2,a) && !IsAllocated(h4,a)) 
));

///////////////////////////////////////////////////////////////////
// Restrictions on legal Isomorphisms
//
// An isomorphism must be consistent with null and
// the existing set of roots.
//
function Iso#MappingPreservesRoots(h1,h2:Heap, a1,a2:Ref) : bool
{
   (a1==null<==>a2==null) && // not allowed to permute null
   (IsRoot(h1,a1) == IsRoot(h2,a2)) && // only roots map to roots
   (IsRoot(h1,a1) ==> a1==a2) // roots mapped by identity
}

///////////////////////////////////////////////////////////////////
// Bounded Isomorphism and Reachability
///////////////////////////////////////////////////////////////////
// The number of steps refers to the number of maximum number of
// non-root objects which may be permuted to find an isomorphism.
//
// Zero steps means that the object under consideration points to
// the object we are trying to reach.
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
// Reach from ar to a in a maximum of 0 steps
//   
function Reach0(h1:Heap,ar,a:Ref) : bool 
{ 
	Edge(h1,ar,a)
 
}

///////////////////////////////////////////////////////////////////
// Reach a1 from ar1 by the same path as a2 from ar2 in a maximum
// of 0 steps
// 
function ReachSame0(h1,h2:Heap,ar1,ar2,a1,a2:Ref) : bool
{
    (Edge(h1,ar1,a1) && Edge(h2,ar2,a2)) 
}

///////////////////////////////////////////////////////////////////
// Isomorphism for heaps from roots to depth n
//
function Heap#Iso0(h1,h2:Heap):bool
{
	(forall ar1,a1,a2:Ref :: 
		{IsAllocated(h1,ar1),$NewOne(h1,a1),$NewOne(h2,a2)}
		Heap#Iso#ReachSameFromRoot0(h1,h2,ar1,a1,a2) ==>
		    Iso#MappingPreservesRoots(h1,h2,a1,a2) && 
			Heap#Iso#ConsistentAliasing0(h1,h2,a1,a2)
	)
}

function Heap#Iso#ConsistentAliasing0(h1,h2:Heap, a1,a2:Ref):bool
{
   (forall ar2,a3,a4:Ref :: {IsAllocated(h1,ar2),$NewOne(h1,a3),$NewOne(h2,a4)} 
		Heap#Iso#ReachSameFromRoot0(h1,h2,ar2,a3,a4) ==> (a1==a3<==>a2==a4))
}

function Heap#Iso#ReachSameFromRoot0(h1,h2:Heap, ar,a1,a2:Ref):bool
{
	IsRoot(h1,ar) && ReachSame0(h1,h2,ar,ar,a1,a2)
}

			
///////////////////////////////////////////////////////////////////
// Reach from ar to a in a maximum of 1 steps
//   
function Reach1(h1:Heap,ar,a:Ref) : bool 
{ 
	Edge(h1,ar,a)
		|| (!Edge(h1,ar,null) && Reach0(h1,ReadField(h1,ar), a)) 
 
}

///////////////////////////////////////////////////////////////////
// Reach a1 from ar1 by the same path as a2 from ar2 in a maximum
// of 1 steps
// 
function ReachSame1(h1,h2:Heap,ar1,ar2,a1,a2:Ref) : bool
{
    (Edge(h1,ar1,a1) && Edge(h2,ar2,a2)) 
		|| (!Edge(h1,ar1,null) && // just an optimisation 
		    !Edge(h2,ar2,null) && // just an optimisation 
		    ReachSame0(h1,h2,ReadField(h1,ar1),ReadField(h2,ar2),a1,a2))
}

///////////////////////////////////////////////////////////////////
// Isomorphism for heaps from roots to depth n
//
function Heap#Iso1(h1,h2:Heap):bool
{
	(forall ar1,a1,a2:Ref :: 
		{IsAllocated(h1,ar1),$NewOne(h1,a1),$NewOne(h2,a2)}
		Heap#Iso#ReachSameFromRoot1(h1,h2,ar1,a1,a2) ==>
		    Iso#MappingPreservesRoots(h1,h2,a1,a2) && 
			Heap#Iso#ConsistentAliasing1(h1,h2,a1,a2)
	)
}

function Heap#Iso#ConsistentAliasing1(h1,h2:Heap, a1,a2:Ref):bool
{
   (forall ar2,a3,a4:Ref :: {IsAllocated(h1,ar2),$NewOne(h1,a3),$NewOne(h2,a4)} 
		Heap#Iso#ReachSameFromRoot1(h1,h2,ar2,a3,a4) ==> (a1==a3<==>a2==a4))
}

function Heap#Iso#ReachSameFromRoot1(h1,h2:Heap, ar,a1,a2:Ref):bool
{
	IsRoot(h1,ar) && ReachSame1(h1,h2,ar,ar,a1,a2)
}

			

var h1:Heap where $GoodHeap(h1);
var h2:Heap where $GoodHeap(h2);
// synthetic procedure with bodies of both versions inlined
procedure CopyList_CopyList(x:Ref,y:Ref)
	modifies h1,h2;
	requires h1 == h2;
	requires Roots(h1) == Allocated(h1);
	requires IsAllocated(h1,x);
	requires IsAllocated(h1,y);
	ensures Roots#Equal(h1,h2);
	ensures Heap#Iso1(h1,h2);
{
	var $t:Ref;

	var x1:Ref;
	var y1:Ref;
	var r1:Ref;
	var xn1:Ref;
	var t1:Ref;

	var x2:Ref;
	var y2:Ref;
	var t2:Ref;
	var xn2:Ref;
	var r2:Ref;

	// version one
	x1 := x;
	y1 := y;
	if(x1 != null)
	{
		if(y1 != null)
		{
			havoc r1; assume !IsAllocated(h1,r1);
			h1:=Allocate(h1,r1); assume $GoodHeap(h1); assume $NewOne(h1,r1);
			xn1 := ReadField(h1,x1);
			call CopyList_1(xn1, r1);
			havoc t1; assume !IsAllocated(h1,t1);
			h1:=Allocate(h1,t1); assume $GoodHeap(h1); assume $NewOne(h1,t1);
			$t:=t1;
			h1:=WriteField(h1,$t,ReadField(h1,r1)); assume $GoodHeap(h1);assume $ModOne(h1,$t);
			$t:=y1;
			h1:=WriteField(h1,$t,t1); assume $GoodHeap(h1);assume $ModOne(h1,$t);
		}

	}


	// version two
	x2 := x;
	y2 := y;
	if(x2 != null)
	{
		havoc t2; assume !IsAllocated(h2,t2);
		h2:=Allocate(h2,t2); assume $GoodHeap(h2); assume $NewOne(h2,t2);
		xn2 := ReadField(h2,x2);
		if(y2 != null)
		{
			havoc r2; assume !IsAllocated(h2,r2);
			h2:=Allocate(h2,r2); assume $GoodHeap(h2); assume $NewOne(h2,r2);
			call CopyList_2(xn2, r2);
			$t:=y2;
			h2:=WriteField(h2,$t,t2); assume $GoodHeap(h2);assume $ModOne(h2,$t);
			$t:=t2;
			h2:=WriteField(h2,$t,ReadField(h2,r2)); assume $GoodHeap(h2);assume $ModOne(h2,$t);
		}

	}

}

// version one of the procedure
procedure CopyList_1(x1:Ref,y1:Ref)
    modifies h1;
	requires IsAllocated(h1,x1);
	requires IsAllocated(h1,y1);
	free ensures NoFrees(old(h1),h1);
	free ensures $GoodHeap(h1);
	free ensures CopyList_1_succ(old(h1),x1,y1,h1);
{
	var $t:Ref;
	var $roots:RefSet;

	var r1:Ref;
	var xn1:Ref;
	var t1:Ref;

	if(x1 != null)
	{
		if(y1 != null)
		{
			havoc r1; assume !IsAllocated(h1,r1);
			h1:=Allocate(h1,r1); assume $GoodHeap(h1); assume $NewOne(h1,r1);
			xn1 := ReadField(h1,x1);
			call CopyList_1(xn1, r1);
			havoc t1; assume !IsAllocated(h1,t1);
			h1:=Allocate(h1,t1); assume $GoodHeap(h1); assume $NewOne(h1,t1);
			$t:=t1;
			h1:=WriteField(h1,$t,ReadField(h1,r1)); assume $GoodHeap(h1);assume $ModOne(h1,$t);
			$t:=y1;
			h1:=WriteField(h1,$t,t1); assume $GoodHeap(h1);assume $ModOne(h1,$t);
		}

	}


	havoc $roots;
	h1 := HeapCons(Data(h1),Allocated(h1),$roots);
	assume $GoodHeap(h1);
}

// version two of the procedure
procedure CopyList_2(x2:Ref,y2:Ref)
    modifies h2;
	requires IsAllocated(h2,x2);
	requires IsAllocated(h2,y2);
	free ensures NoFrees(old(h2),h2);
	free ensures $GoodHeap(h2);
	free ensures CopyList_2_succ(old(h2),x2,y2,h2);
{
	var $t:Ref;
	var $roots:RefSet;

	var t2:Ref;
	var xn2:Ref;
	var r2:Ref;

	if(x2 != null)
	{
		havoc t2; assume !IsAllocated(h2,t2);
		h2:=Allocate(h2,t2); assume $GoodHeap(h2); assume $NewOne(h2,t2);
		xn2 := ReadField(h2,x2);
		if(y2 != null)
		{
			havoc r2; assume !IsAllocated(h2,r2);
			h2:=Allocate(h2,r2); assume $GoodHeap(h2); assume $NewOne(h2,r2);
			call CopyList_2(xn2, r2);
			$t:=y2;
			h2:=WriteField(h2,$t,t2); assume $GoodHeap(h2);assume $ModOne(h2,$t);
			$t:=t2;
			h2:=WriteField(h2,$t,ReadField(h2,r2)); assume $GoodHeap(h2);assume $ModOne(h2,$t);
		}

	}


	havoc $roots;
	h2 := HeapCons(Data(h2),Allocated(h2),$roots);
	assume $GoodHeap(h2);
}

// equivalence mutual summary for CopyList
function CopyList_1_succ(h1:Heap,x:Ref,y:Ref,h2:Heap):bool;
function CopyList_2_succ(h1:Heap,x:Ref,y:Ref,h2:Heap):bool;  

axiom (forall 
		h1,h2,h3,h4:Heap,
		x1:Ref,y1:Ref,
		x2:Ref,y2:Ref ::
   {CopyList_1_succ(h1,x1,y1,h3), CopyList_2_succ(h2,x2,y2,h4)}
      CopyList_1_succ(h1,x1,y1,h3) 
   && CopyList_2_succ(h2,x2,y2,h4) 
   && CopyList_Call#Iso(h1,x1,y1,h2,x2,y2) 
   	==>    Heap#SameMods(h1,h3,h2,h4)
   	    && CopyList_Roots#Updated(h1,x1,y1,h3)
   	    && CopyList_Roots#Updated(h2,x2,y2,h4)
   	    && CopyList_Call#Eq(h1,x1,y1,h2,x2,y2)
   );

function CopyList_Roots#Updated(h1:Heap,x:Ref,y:Ref,h3:Heap) : bool
{
	(forall a:Ref :: 
		IsRoot(h3,a) <==> 
			   IsRoot(h1,a) 
			|| (!IsAllocated(h1,a) && IsAllocated(h3,a)) 
			|| CopyList_Call#Reach(h1,x,y, a))
}

function CopyList_Call#Reach(h1:Heap, x:Ref,y:Ref, a:Ref):bool;
axiom (forall h1:Heap, x:Ref,y:Ref, a:Ref :: 
	CopyList_Call#Reach(h1, x,y, a) ==
	(
		(exists ar:Ref :: IsRoot(h1,ar) && Reach1(h1,ar,a)) ||  
            Reach1(h1,x,a) || x==a || Reach1(h1,y,a) || y==a
	));

function CopyList_Call#Iso(h1:Heap, x1:Ref,y1:Ref, h2:Heap, x2:Ref,y2:Ref):bool;
axiom (forall h1,h2:Heap, x1:Ref,y1:Ref,x2:Ref,y2:Ref :: 
	CopyList_Call#Iso(h1, x1,y1, h2, x2,y2) ==
	(
	    Roots#Equal(h1,h2) &&  
		Heap#Iso1(h1,h2) && 
		CopyList_CallRef#Iso(h1, x1,y1, h2, x2,y2, x1, x2)
 && CopyList_CallRef#Iso(h1, x1,y1, h2, x2,y2, y1, y2)

	));

function CopyList_CallRef#Iso(h1:Heap, x1:Ref,y1:Ref, h2:Heap, x2:Ref,y2:Ref, ar1,ar2:Ref):bool
{
    Iso#MappingPreservesRoots(h1,h2,ar1,ar2) &&
	(forall a1,a2:Ref :: {$NewOne(h1,a1),$NewOne(h2,a2)}
    	ReachSame1(h1,h2,ar1,ar2,a1,a2) ==>  
        	Heap#Iso#ConsistentAliasing1(h1,h2,a1,a2) && 
        	Iso#MappingPreservesRoots(h1,h2,a1,a2)
   	    	&& (forall a3,a4:Ref :: {$NewOne(h1,a3),$NewOne(h2,a4)} 
   	    		   ReachSame1(h1,h2,x1,x1,a3,a4) ==> (a1==a3<==>a2==a4))
   	    	&& (forall a3,a4:Ref :: {$NewOne(h1,a3),$NewOne(h2,a4)} 
   	    		   ReachSame1(h1,h2,y1,y1,a3,a4) ==> (a1==a3<==>a2==a4))
	)
}

//////////////////////////////////////////////////////////////////////////////
// If we found that the parameters were isomorphic we assume they are equal
function CopyList_Call#Eq(h1:Heap, x1:Ref,y1:Ref, h2:Heap, x2:Ref,y2:Ref):bool;
axiom (forall h1,h2:Heap, x1:Ref,y1:Ref,x2:Ref,y2:Ref :: 
	CopyList_Call#Eq(h1, x1,y1, h2, x2,y2) ==>
	(
	    Roots#Equal(h1,h2) &&
		(forall ar,a1,a2:Ref :: 
  				{$ModOne(h1,ar),$NewOne(h1,a1),$NewOne(h2,a2)}
  				{$ModOne(h2,ar),$NewOne(h1,a1),$NewOne(h2,a2)}
  				IsRoot(h1,ar) ==> ReachSame1(h1,h2,ar,ar,a1,a2) ==> a1==a2) &&
		x1 == x2 &&
		(forall a1,a2:Ref :: {$NewOne(h1,a1),$NewOne(h2,a2)}
        	ReachSame1(h1,h2,x1,x2,a1,a2) ==> a1==a2) && 
		y1 == y2 &&
		(forall a1,a2:Ref :: {$NewOne(h1,a1),$NewOne(h2,a2)}
        	ReachSame1(h1,h2,y1,y2,a1,a2) ==> a1==a2)
	));

