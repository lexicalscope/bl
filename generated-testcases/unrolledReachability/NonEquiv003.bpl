/////////////////////////////////////////////////////////////////////////////
// Fields
type Field alpha;
const unique $Alloc:Field bool;
const unique $F:Field Ref;
const unique $G:Field Ref;
axiom (forall<alpha> $f:Field alpha :: $f==$Alloc || $f == $F || $f == $G);

/////////////////////////////////////////////////////////////////////////////
// Heap
type Ref;
const $Null : Ref;
type Heap = <alpha>[Ref, Field alpha] alpha;


/////////////////////////////////////////////////////////////////////////////
// Heap Manipulation Functions			
function {:inline true} $Read<alpha>($h:Heap, $a:Ref, $f:Field alpha) : alpha {$h[$a,$f]}
function {:inline true} $Write<alpha>($h:Heap, $a:Ref, $f:Field alpha, $v:alpha) : Heap {$h[$a,$f := $v]}
function {:inline true} $Edge($h:Heap, $a1:Ref, $f:Field Ref, $a2:Ref) : bool {$Read($h,$a1,$f)==$a2}
function {:inline true} $Neighbour($h:Heap, $a1:Ref, $a2:Ref) : bool {(exists $f:Field Ref :: $Edge($h,$a1,$f,$a2))}
function {:inline true} $Allocated($h:Heap, $a:Ref) : bool {$Read($h, $a, $Alloc)}
function {:inline true} $Allocate($h:Heap, $a:Ref) : Heap {$Write($h,$a,$Alloc,true)}

function $EmptyHeap() : Heap;
axiom (forall $h:Heap :: $h==$EmptyHeap() ==> (forall $a:Ref :: $h[$a,$Alloc] == false)); 

/////////////////////////////////////////////////////////////////////////////
// Well Formed Heap
function $GoodHeap($h:Heap) : bool;
axiom (forall $h:Heap :: $GoodHeap($h) ==> (forall $a:Ref,$f:Field Ref :: !$Allocated($h, $a) ==> $Edge($h,$a,$f, $Null)));

/////////////////////////////////////////////////////////////////////////////
// Peano Numbers
type Fuel;
function $Zero():Fuel;
function $Succ(Fuel):Fuel;

/////////////////////////////////////////////////////////////////////////////
// Bounded Unroll Reachability
function $Reach($fuel:Fuel, $h:Heap, $a1:Ref, $a2:Ref) : bool;

// synonym
axiom (forall $fuel:Fuel, $h:Heap, $a1:Ref, $a2:Ref :: {$Reach($Succ($fuel),$h,$a1,$a2)}
	$Reach($Succ($fuel), $h,$a1,$a2) == $Reach($fuel, $h,$a1,$a2));
// definition
axiom (forall $fuel:Fuel, $h:Heap, $a1:Ref, $a2:Ref :: {$Reach($Succ($fuel),$h,$a1,$a2)}
	$Reach($Succ($fuel), $h,$a1,$a2) == 
		($a1==$a2 || (exists $f:Field Ref :: $Reach($fuel,$h,$Read($h,$a1,$f),$a2))));
			//(exists $a3:Ref, $f:Field Ref :: $Edge($h,$a1,$f,$a3) && $a3==$a2 ))); //&& $Reach($fuel, $h,$a3,$a2) 


/////////////////////////////////////////////////////////////////////////////
// Test for heap manipulation
var $h:Heap where $GoodHeap($h);

procedure $TestCanFindEdgeInHeap() 
	modifies $h;
{
   var x,y:Ref;
   havoc x; assume !$Allocated($h, x);
   $h := $Allocate($h, x);
   havoc y; assume !$Allocated($h, y);
   $h := $Allocate($h, y);
   $h := $Write($h, x, $F, y); 
   assert $Neighbour($h, x, y);
}

procedure $TestCanDetermineReachability() 
	modifies $h;
{
   var x,y:Ref;
   havoc x; assume !$Allocated($h, x);
   $h := $Allocate($h, x);
   havoc y; assume !$Allocated($h, y);
   $h := $Allocate($h, y);
   $h := $Write($h, x, $F, y);
   assert $Reach($Succ($Succ($Succ($Succ($Zero())))), $h, x, y);
}

procedure $TestReachabilityBeyondFuelIsUnknown() 
	modifies $h;
{
   var a,b,c,d,e,f:Ref;
   havoc a; assume !$Allocated($h, a);
   $h := $Allocate($h, a);
   havoc b; assume !$Allocated($h, b);
   $h := $Allocate($h, b);
   havoc c; assume !$Allocated($h, c);
   $h := $Allocate($h, c);
   havoc d; assume !$Allocated($h, d);
   $h := $Allocate($h, d);
   havoc e; assume !$Allocated($h, e);
   $h := $Allocate($h, e);
   havoc f; assume !$Allocated($h, f);
   $h := $Allocate($h, f);
   $h := $Write($h, a, $F, b);
   $h := $Write($h, b, $F, c);
   $h := $Write($h, c, $F, d);
   $h := $Write($h, d, $F, e);
   $h := $Write($h, e, $F, f);
   assert $Reach($Succ($Succ($Succ($Succ($Zero())))), $h, a, f);
} 			