package com.lexicalscope.bl.equiv;

import static com.lexicalscope.bl.equiv.BoogieResult.verifiedWith;
import static com.lexicalscope.bl.equiv.BoogieResult.verifiedWithNoErrors;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.lexicalscope.bl.parser.BlAntlrRule;
import com.lexicalscope.bl.parser.BlParser.MultiversionContext;
import com.lexicalscope.bl.parser.UseGrammar;

public abstract class TestVerification {
    @Rule public TemporaryFolder folder = new TemporaryFolder();
    @Rule public BlAntlrRule<MultiversionContext> p = new BlAntlrRule<MultiversionContext>() {
        @Override protected MultiversionContext parseNow() {
            return parser().multiversion();
        }
    };

    @Test @UseGrammar("Empty.bl") public void emptyVerifies() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWithNoErrors(0));
    }

    @Test @UseGrammar("Swap.bl") public void heapSwapAndStackSwapDifferent() throws IOException, InterruptedException {
        //Assume.assumeTrue(false);
        assertThat(verifyWithBoogie(), verifiedWithNoErrors(3));
    }

    @Test @UseGrammar("Call001.bl") public void justACall() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWithNoErrors(6));
    }

    @Test @UseGrammar("Call002.bl") public void twoCalls() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWithNoErrors(6));
    }

    @Test @UseGrammar("Frame001.bl") public void allocationMovedPastCall() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWithNoErrors(6));
    }

    @Test @UseGrammar("Frame002.bl") public void allocationsMovedPassedCallThatNeedsFrameInfo() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWithNoErrors(6));
    }

    @Test @UseGrammar("Frame003.bl") public void allocationMovedPassedCallThatIsNotEquivalent() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(5, 1));
    }

    @Test @UseGrammar("Frame004.bl") public void methodReadsToMuch() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(1, 2));
    }

    @Test @UseGrammar("Frame005.bl") public void methodWritesToMuch() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(1, 2));
    }

    @Test @UseGrammar("Frame006.bl") public void unsoundnessFromInconsistentIsomorphisms() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(5, 2));
    }

    @Test @UseGrammar("NonEquiv001.bl") public void assignsDifferentLocations() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(2, 1));
    }

    @Test @UseGrammar("NonEquiv002.bl") public void createsDifferentLengthLists() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(2, 1));
    }

    @Test @UseGrammar("NonEquiv003.bl") public void aliasedDifferentFromNonAliased() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(2, 1));
    }

    @Test @UseGrammar("DifferentParameters.bl") public void differentParameters() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(2, 1));
    }

    @Test @UseGrammar("Conditional001.bl") public void conditionalReversal() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(9, 0));
    }

    @Test @UseGrammar("Conditional002.bl") public void nonEquivalentConditionalBodies() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(8, 2));
    }

    @Test @UseGrammar("PassNew001.bl") public void passNewThenUseItCheckedInAConditional() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(6, 0));
    }

    @Test @UseGrammar("PassNew002.bl") public void passNewThenUseWronglyInAConditional() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(5, 1));
    }

    @Test @UseGrammar("PassNew003.bl") public void passNewThenUseItUncheckedInAConditional() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(6, 0));
    }

    @Test @UseGrammar("PassNew004.bl") public void passNewIndirectlyThenUseItInAConditional() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(6, 0));
    }

    @Test @UseGrammar("PassNew005.bl") public void passNewIndirectly() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(6, 0));
    }

    @Test @UseGrammar("PassNew006.bl") public void passNewViaNonParameterVariable() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(6, 0));
    }

    @Test @UseGrammar("PassNew007.bl") public void passNewViaNonParameterVariableSeveralTimes() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(6, 0));
    }

    @Test @UseGrammar("Recursion001.bl") public void copyAList() throws IOException, InterruptedException {
        assertThat(verifyWithBoogie(), verifiedWith(3, 0));
    }

    BoogieResult verifyWithBoogie() throws IOException, InterruptedException {
        final File outputDir = outputDirectory();
        outputDir.mkdirs();

        return new BoogieVerifier().execBoogie(new BoogieGenerator(outputDir, verificationStrategy()).writeBoogieForVersions(p));
    }

    abstract File outputDirectory();
    abstract VerificationStrategy verificationStrategy();
}
