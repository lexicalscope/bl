package com.lexicalscope.bl.equiv;

import java.util.Arrays;

public class StatementBuilder {
    public static class ConditionalStatementBuilder {
        private final BoolExpression condition;

        public ConditionalStatementBuilder(final BoolExpression condition) {
            this.condition = condition;
        }

        public ConditionalStatementBuilderThen then(final Statement ... statements) {
            return new ConditionalStatementBuilderThen(condition, statements);
        }
    }

    public static class ConditionalStatementBuilderThen {
        private final BoolExpression condition;
        private final Statement[] thenStatements;

        public ConditionalStatementBuilderThen(final BoolExpression condition, final Statement[] thenStatements) {
            this.condition = condition;
            this.thenStatements = thenStatements;
        }

        public ConditionalStatementBuilderElse elsethen(final Statement ... elseStatements) {
            return new ConditionalStatementBuilderElse(condition, thenStatements, elseStatements);
        }
    }

    public static class ConditionalStatementBuilderElse {
        private final BoolExpression condition;
        private final Statement[] thenStatements;
        private final Statement[] elseStatements;

        public ConditionalStatementBuilderElse(
                final BoolExpression condition,
                final Statement[] thenStatements,
                final Statement[] elseStatements) {
                    this.condition = condition;
                    this.thenStatements = thenStatements;
                    this.elseStatements = elseStatements;
        }

        public ConditionalStatement mk() {
            return new ConditionalStatement(condition, Arrays.asList(thenStatements), Arrays.asList(elseStatements));
        }

    }

    public static ConditionalStatementBuilder conditional(final BoolExpression condition) {
        return new ConditionalStatementBuilder(condition);
    }
}
