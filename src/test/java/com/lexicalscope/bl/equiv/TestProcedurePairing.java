package com.lexicalscope.bl.equiv;

import static com.lexicalscope.MatchersJ8.containsMatching;
import static com.lexicalscope.bl.equiv.ProcedurePairsCollector.procedurePairs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

import java.io.IOException;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.lexicalscope.MatchersJ8;
import com.lexicalscope.bl.parser.BlAntlrRule;
import com.lexicalscope.bl.parser.BlParser.MultiversionContext;
import com.lexicalscope.bl.parser.UseGrammar;

public class TestProcedurePairing {
    @Rule public TemporaryFolder folder = new TemporaryFolder();
    @Rule public BlAntlrRule<MultiversionContext> p = new BlAntlrRule<MultiversionContext>() {
        @Override protected MultiversionContext parseNow() {
            return parser().multiversion();
        }
    };

    @Test @UseGrammar("ProcedurePairs001.bl") public void proceduresArePaired() throws IOException, InterruptedException {
        assertThat(procedurePairs(p.tree()),
                containsMatching(pairCalled("FirstProcedure"), pairCalled("SecondProcedure"), pairCalled("ThirdProcedure")));
    }

    @Test @UseGrammar("ProcedurePairs002.bl") public void localsAreFound() throws IOException, InterruptedException {
        assertThat(procedurePairs(p.tree()),
                containsMatching(ver1(locals(containsMatching(equalTo("t"), equalTo("s"))))));
    }

    @Test @UseGrammar("ProcedurePairs003.bl") public void statementsAreFound() throws IOException, InterruptedException {
        assertThat(procedurePairs(p.tree()),
                containsMatching(ver1(statements(containsMatching(
                        Statement.class,
                        equalTo(new LocalUpdateStatement("t", "x")),
                        equalTo(new HeapUpdateStatement("t", "x")),
                        equalTo(new HeapUpdateStatement("t", new HeapReadExpression("x"))),
                        equalTo(new LocalUpdateStatement("t", new HeapReadExpression("x"))),
                        equalTo(new HeapAllocStatement("t")),
                        equalTo(new ProcedureCallStatement("Foo", "t"))
                        )))));
    }

    @Test @UseGrammar("ProcedurePairs004.bl") public void modifiesIsFound() throws IOException, InterruptedException {
        assertThat(procedurePairs(p.tree()),
                containsMatching(ver1(modifies(containsMatching(
                        Expression.class,
                        equalTo(new LocalReadExpression("x"))
                        )))));
    }

    @Test @UseGrammar("ProcedurePairs005.bl") public void readsIsFound() throws IOException, InterruptedException {
        assertThat(procedurePairs(p.tree()),
                containsMatching(ver1(reads(containsMatching(
                        Expression.class,
                        equalTo(new LocalReadExpression("x"))
                        )))));
    }

    @Test @UseGrammar("ProcedurePairs006.bl") public void newsCanBeCounted() throws IOException, InterruptedException {
        assertThat(procedurePairs(p.tree()),
                containsMatching(ver1(newsCount(equalTo(1))), ver1(newsCount(equalTo(4)))));
    }

    @Test @UseGrammar("ProcedurePairs007.bl") public void conditionalStatementsAreFound() throws IOException, InterruptedException {
        assertVer1Matches(statements(containsMatching(
                        Statement.class,
                        equalTo(new HeapAllocStatement("t")),
                        equalTo(StatementBuilder.
                                conditional(new EqualToExpression("x", "null")).
                                then(new HeapUpdateStatement("x", "t")).
                                elsethen(new HeapUpdateStatement("y", "t")).mk())
                        )));
    }

    @Test @UseGrammar("ProcedurePairs008.bl") public void emptyModifiesIsFound() throws IOException, InterruptedException {
        assertVer1Matches(modifies(equalTo(new EmptyModifies())));
    }


    @Test @UseGrammar("ProcedurePairs009.bl") public void noModifiesIsFound() throws IOException, InterruptedException {
        assertVer1Matches(modifies(nullValue()));
    }


    @Test @UseGrammar("ProcedurePairs010.bl") public void singletonModifiesIsFound() throws IOException, InterruptedException {
        assertVer1Matches(modifies(equalTo(new ModifiesSet(new LocalReadExpression("x")))));
    }

    @Test @UseGrammar("ProcedurePairs011.bl") public void setExpressionIsFound() throws IOException, InterruptedException {
        assertVer1Matches(modifies(equalTo(new ModifiesSet(new LocalReadExpression("x"), new LocalReadExpression("y")))));
    }

    void assertVer1Matches(final Matcher<ProcedureBody> ver1Matcher) {
        assertThat(procedurePairs(p.tree()),
                containsMatching(ver1(ver1Matcher)));
    }

    private Matcher<ProcedurePair> ver1(final Matcher<ProcedureBody> bodyMatcher) {
        return MatchersJ8.featureMatcher(
            "ver1",
            pair -> pair.ver1(),
            bodyMatcher
        );
    }

    private Matcher<ProcedureBody> locals(final Matcher<Iterable<? extends String>> localsMatcher) {
        return MatchersJ8.featureMatcher(
            "locals",
            body -> body.getLocals(),
            localsMatcher
        );
    }

    private Matcher<ProcedureBody> newsCount(final Matcher<Integer> newsMatcher) {
        return MatchersJ8.featureMatcher(
            "locals",
            body -> body.getNewsCount(),
            newsMatcher
        );
    }

    private Matcher<ProcedureBody> statements(final Matcher<Iterable<? extends Statement>> statementsMatcher) {
        return MatchersJ8.featureMatcher(
            "statements",
            body -> body.getStatements(),
            statementsMatcher
        );
    }

    private Matcher<ProcedureBody> modifies(final Matcher<? super Modifies> expressionsMatcher) {
        return MatchersJ8.featureMatcher(
            "modifies",
            body -> body.getModifies(),
            expressionsMatcher
        );
    }

    private Matcher<ProcedureBody> reads(final Matcher<Iterable<? extends Expression>> expressionsMatcher) {
        return MatchersJ8.featureMatcher(
            "reads",
            body -> body.getReads(),
            expressionsMatcher
        );
    }

    private Matcher<ProcedurePair> pairCalled(final String procedureName) {
        return MatchersJ8.matcher(
                description -> description.appendText("pair of procedures called ").appendValue(procedureName),
                (item, mismatchDescription) -> mismatchDescription.appendText("pair of procedures called ").appendValue(item.name()),
                item -> item.name().equals(procedureName)
        );
    }
}
