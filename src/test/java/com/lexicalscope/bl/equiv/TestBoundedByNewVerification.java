package com.lexicalscope.bl.equiv;

import java.io.File;


public class TestBoundedByNewVerification extends TestVerification {
    @Override
    BoundedByNewVerificationStrategy verificationStrategy() {
        return new BoundedByNewVerificationStrategy();
    }

    @Override
    File outputDirectory() {
        return new File("generated-testcases/boundedByNew");
    }
}
