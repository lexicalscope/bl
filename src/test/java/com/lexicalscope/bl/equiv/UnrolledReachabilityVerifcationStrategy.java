package com.lexicalscope.bl.equiv;

import java.io.File;

public class UnrolledReachabilityVerifcationStrategy implements VerificationStrategy {
    @Override public BoogieWriter boogieWriter(final File output) {
        return new StringTemplateBoogieWriter(output, "unrolledReachabilityTemplates/equiv.stg");
    }
}
