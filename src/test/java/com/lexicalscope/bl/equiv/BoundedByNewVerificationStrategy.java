package com.lexicalscope.bl.equiv;

import java.io.File;

public class BoundedByNewVerificationStrategy implements VerificationStrategy {
    @Override public BoogieWriter boogieWriter(final File output) {
        return new StringTemplateBoogieWriter(output, "boundedByNewTemplates/equiv.stg");
    }
}
