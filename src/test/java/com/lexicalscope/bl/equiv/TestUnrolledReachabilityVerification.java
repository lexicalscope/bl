package com.lexicalscope.bl.equiv;

import java.io.File;


public abstract class TestUnrolledReachabilityVerification extends TestVerification {
    @Override
    VerificationStrategy verificationStrategy() {
        return new UnrolledReachabilityVerifcationStrategy();
    }

    @Override
    File outputDirectory() {
        return new File("generated-testcases/unrolledReachability");
    }
}
