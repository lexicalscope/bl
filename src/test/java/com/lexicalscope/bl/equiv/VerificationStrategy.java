package com.lexicalscope.bl.equiv;

import java.io.File;

public interface VerificationStrategy {
    BoogieWriter boogieWriter(File output);
}
