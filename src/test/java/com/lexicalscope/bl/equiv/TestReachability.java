package com.lexicalscope.bl.equiv;

import static com.lexicalscope.MatchersJ8.containsMatching;
import static com.lexicalscope.MatchersJ8.featureMatcher;
import static com.lexicalscope.MatchersJ8.matcher;
import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.Matcher;
import org.junit.Test;

import com.lexicalscope.bl.equiv.Reachability.ReachStep;

public class TestReachability {
    @Test public void countAndListConsistent()
    {
        assertThat(new Reachability(5), steps(
                containsMatching(
                        stepsCount(0),
                        stepsCount(1),
                        stepsCount(2),
                        stepsCount(3),
                        stepsCount(4),
                        stepsCount(5),
                        stepsCount(6))));
    }

    private Matcher<ReachStep> stepsCount(final int i) {
        return matcher(
                describeTo -> describeTo.appendText("step ").appendValue(i),
                (r, describeMismatch) -> describeMismatch.appendText("step ").appendValue(r.getStep()),
                r -> r.getStep() == i);
    }

    private Matcher<Reachability> steps(final Matcher<Iterable<? extends ReachStep>> submatcher) {
        return featureMatcher(
                "reachability",
                r -> r.getSteps(),
                submatcher);
    }
}
