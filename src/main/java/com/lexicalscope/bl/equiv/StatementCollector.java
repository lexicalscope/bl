package com.lexicalscope.bl.equiv;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.lexicalscope.bl.parser.BlBaseListener;
import com.lexicalscope.bl.parser.BlBaseVisitor;
import com.lexicalscope.bl.parser.BlParser.AllocateUpdateContext;
import com.lexicalscope.bl.parser.BlParser.BlockContext;
import com.lexicalscope.bl.parser.BlParser.ConditionalContext;
import com.lexicalscope.bl.parser.BlParser.ExpressionUpdateContext;
import com.lexicalscope.bl.parser.BlParser.HeapUpdateContext;
import com.lexicalscope.bl.parser.BlParser.LocalUpdateContext;
import com.lexicalscope.bl.parser.BlParser.ProcedureCallContext;

final class StatementCollector extends BlBaseVisitor<Void> {
    private final List<Statement> result = new ArrayList<Statement>();
    private final StatementCollectionStrategy statementStrategy;

    public interface StatementCollectionStrategy {

        void conditional(StatementCollector statementCollector, ConditionalContext ctx);

    }

    public static class StatementTreeStrategy implements StatementCollectionStrategy {
        @Override public void conditional(final StatementCollector statementCollector, final ConditionalContext ctx) {
            statementCollector.result.add(new ConditionalStatement(
                    ExpressionFactory.boolExpression(ctx.bexpression()),
                    new StatementCollector().collect(ctx.then),
                    new StatementCollector().collect(ctx.elsethen)));
        }
    }

    public static class AtomicStatementStrategy implements StatementCollectionStrategy {
        @Override public void conditional(final StatementCollector statementCollector, final ConditionalContext ctx) {
            statementCollector.visitChildren(ctx);
        }
    }

    public StatementCollector() {
        this(new StatementTreeStrategy());
    }

    public StatementCollector(final StatementCollectionStrategy statementStrategy) {
        this.statementStrategy = statementStrategy;
    }

    public List<Statement> getResult() {
        return result;
    }

    @Override public Void visitHeapUpdate(final HeapUpdateContext ctx) {
        result.add(new HeapUpdateStatement(ctx));
        return null;
    }

    @Override public Void visitLocalUpdate(final LocalUpdateContext localUpdateContext) {
        final ParseTreeWalker parseTreeWalker = new ParseTreeWalker();
        parseTreeWalker.walk(new BlBaseListener(){
            @Override public void enterExpressionUpdate(final ExpressionUpdateContext expressionUpdateContext) {
                result.add(new LocalUpdateStatement(localUpdateContext, expressionUpdateContext));
            }
            @Override public void enterAllocateUpdate(final AllocateUpdateContext allocateUpdateContext) {
                result.add(new HeapAllocStatement(localUpdateContext, allocateUpdateContext));
            }
        }, localUpdateContext);
        return null;
    }

    @Override public Void visitProcedureCall(final ProcedureCallContext ctx) {
        result.add(new ProcedureCallStatement(ctx));
        return null;
    }

    @Override public Void visitConditional(final ConditionalContext ctx) {
        statementStrategy.conditional(this, ctx);
        return null;
    }

    public List<Statement> collect(final BlockContext block) {
        if(block != null) {
            this.visit(block);
        }
        return getResult();
    }

    public static List<Statement> collectAtoms(final BlockContext block) {
        return new StatementCollector(new AtomicStatementStrategy()).collect(block);
    }
}