package com.lexicalscope.bl.equiv;

public interface Statement {
    String getType();
    boolean isAlloc();
    @Override int hashCode();
    @Override boolean equals(Object obj);
}
