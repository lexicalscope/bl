package com.lexicalscope.bl.equiv;

import java.util.Collections;
import java.util.Iterator;

public class EmptyModifies implements Modifies {
    @Override public Iterator<Expression> iterator() {
        return Collections.emptyIterator();
    }

    @Override public int hashCode() {
        return getClass().hashCode();
    }

    @Override public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return true;
    }
}
