package com.lexicalscope.bl.equiv;

import static com.lexicalscope.bl.equiv.ExpressionFactory.expression;

import com.lexicalscope.bl.parser.BlParser.ExpressionUpdateContext;
import com.lexicalscope.bl.parser.BlParser.LocalUpdateContext;

public class LocalUpdateStatement implements Statement, LocalAssignment {
    private final String lhsVar;
    private final Expression expression;

    public LocalUpdateStatement(final LocalUpdateContext localUpdateContext, final ExpressionUpdateContext expressionUpdateContext) {
        this(localUpdateContext.local.getText(), expression(expressionUpdateContext.expr));
    }

    public LocalUpdateStatement(final String lhsVar, final String rhsVar) {
        this(lhsVar, new LocalReadExpression(rhsVar));
    }

    public LocalUpdateStatement(final String lhsVar, final Expression expression) {
        this.lhsVar = lhsVar;
        this.expression = expression;
    }

    @Override public String getLhsVar() {
        return lhsVar;
    }

    public Expression getExpression() {
        return expression;
    }

    @Override public String getType() {
        return "LocalUpdate";
    }

    @Override public boolean isAlloc() {
        return false;
    }

    @Override public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expression == null) ? 0 : expression.hashCode());
        result = prime * result + ((lhsVar == null) ? 0 : lhsVar.hashCode());
        return result;
    }

    @Override public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocalUpdateStatement other = (LocalUpdateStatement) obj;
        if (expression == null) {
            if (other.expression != null) {
                return false;
            }
        } else if (!expression.equals(other.expression)) {
            return false;
        }
        if (lhsVar == null) {
            if (other.lhsVar != null) {
                return false;
            }
        } else if (!lhsVar.equals(other.lhsVar)) {
            return false;
        }
        return true;
    }

    @Override public String toString() {
        return "LocalUpdateStatement [lhsVar=" + lhsVar + ", expression=" + expression + "]";
    }
}
