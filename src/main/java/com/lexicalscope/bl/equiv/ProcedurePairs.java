package com.lexicalscope.bl.equiv;

import java.util.Iterator;
import java.util.List;

import com.lexicalscope.Jc;

public class ProcedurePairs implements Iterable<ProcedurePair> {
    private final List<ProcedurePair> pairs;

    public ProcedurePairs(final List<ProcedurePair> pairs) {
        this.pairs = pairs;
    }

    @Override public Iterator<ProcedurePair> iterator() {
        return pairs.iterator();
    }

    public int maxNewsCount() {
        return Jc.$(pairs).foldlInt(0, (p,a) -> Math.max(a, p.maxNewsCount()));
    }

    public Reachability reachability() {
        return new Reachability(maxNewsCount());
    }
}
