package com.lexicalscope.bl.equiv;

import static com.lexicalscope.Jc.$;

import java.util.List;

import com.lexicalscope.bl.parser.BlParser.ProcedureContext;
import com.lexicalscope.bl.parser.BlParser.SpecificationContext;

public class ProcedurePair {
    private final String name;
    private final ProcedureContext first;
    private final ProcedureContext second;

    public ProcedurePair(final String name, final ProcedureContext first, final ProcedureContext second) {
        assert name.equals(first.IDENTIFIER().getText());
        assert name.equals(second.IDENTIFIER().getText());
        assert paramNames(first).equals(paramNames(second));

        this.name = name;
        this.first = first;
        this.second = second;
    }

    public String name() {
        return name;
    }

    public List<String> params() {
        return paramNames(first);
    }

    private List<String> paramNames(final ProcedureContext procedure) {
        return $(procedure.paramsDecl().vars).map(p -> p.IDENTIFIER().getText())._$();
    }

    public ProcedureBody ver1() {
        return new ProcedureBody(params(), specification(first), first.block());
    }

    public ProcedureBody ver2() {
        return new ProcedureBody(params(), specification(second), second.block());
    }

    private SpecificationContext specification(final ProcedureContext procedure) {
        return procedure.specification();
    }

    public Reachability reachability() {
        return new Reachability(maxNewsCount());
    }

    public int maxNewsCount() {
        return Math.max(ver1().getNewsCount(), ver2().getNewsCount());
    }
}
