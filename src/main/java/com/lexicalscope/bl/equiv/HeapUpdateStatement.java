package com.lexicalscope.bl.equiv;

import static com.lexicalscope.bl.equiv.ExpressionFactory.expression;

import com.lexicalscope.bl.parser.BlParser.HeapUpdateContext;

public class HeapUpdateStatement implements Statement {
    private final Expression lhs;
    private final Expression rhs;

    public HeapUpdateStatement(final HeapUpdateContext u) {
        this(expression(u.lhs), expression(u.rhs));
    }

    public HeapUpdateStatement(final String lhsVar, final String rhsVar) {
        this(new LocalReadExpression(lhsVar), new LocalReadExpression(rhsVar));
    }

    public HeapUpdateStatement(final String lhsVar, final Expression rhs) {
        this(new LocalReadExpression(lhsVar), rhs);
    }

    public HeapUpdateStatement(final Expression lhs, final Expression rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public Expression getLhs() {
        return lhs;
    }

    public Expression getRhs() {
        return rhs;
    }

    @Override public String getType() {
        return "HeapUpdate";
    }

    @Override public boolean isAlloc() {
        return false;
    }

    @Override public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((lhs == null) ? 0 : lhs.hashCode());
        result = prime * result + ((rhs == null) ? 0 : rhs.hashCode());
        return result;
    }

    @Override public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HeapUpdateStatement other = (HeapUpdateStatement) obj;
        if (lhs == null) {
            if (other.lhs != null) {
                return false;
            }
        } else if (!lhs.equals(other.lhs)) {
            return false;
        }
        if (rhs == null) {
            if (other.rhs != null) {
                return false;
            }
        } else if (!rhs.equals(other.rhs)) {
            return false;
        }
        return true;
    }

    @Override public String toString() {
        return "HeapUpdateStatement [lhs=" + lhs + ", rhs=" + rhs + "]";
    }
}
