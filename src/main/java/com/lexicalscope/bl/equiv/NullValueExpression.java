package com.lexicalscope.bl.equiv;

public class NullValueExpression implements Expression {
    @Override public String getType() {
        return "NullValue";
    }

    @Override public boolean equals(final Object obj) {
        return obj != null && obj.getClass().equals(getClass());
    }

    @Override public int hashCode() {
        return getClass().hashCode();
    }

    @Override public String toString() {
        return "NullValueExpression []";
    }
}
