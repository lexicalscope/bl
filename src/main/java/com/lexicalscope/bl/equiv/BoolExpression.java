package com.lexicalscope.bl.equiv;

public interface BoolExpression {
    String getType();
    @Override int hashCode();
    @Override boolean equals(Object obj);
}
