package com.lexicalscope.bl.equiv;

import java.util.ArrayList;
import java.util.List;

public class Reachability {
    public interface ReachStep {
        int getStep();
        boolean isZero();
    }

    public class ReachStepZero implements ReachStep {
        @Override public int getStep() {
            return 0;
        }

        @Override public boolean isZero() {
            return true;
        }
    }

    public class ReachStepNonZero implements ReachStep {
        private final int next;
        private final int step;

        public ReachStepNonZero(final int step, final int next) {
            this.step = step;
            this.next = next;
        }

        public int getNext() {
            return next;
        }

        @Override public int getStep() {
            return step;
        }

        public List<ReachStep> getSmaller() {
            return limitSteps.get(getStep());
        }

        @Override public boolean isZero() {
            return false;
        }

        @Override public String toString() {
            return "Reach Step " + getStep();
        }
    }

    private final int count;
    private final List<List<ReachStep>> limitSteps;

    public Reachability(final int maxNews) {
        this.count = maxNews + 1;
        limitSteps = new ArrayList<>();
        limitSteps.add(new ArrayList<ReachStep>());
        final List<ReachStep> result = new ArrayList<>();
        for (int i = 0; i <= count; i++) {
            result.add(i==0 ? new ReachStepZero() : new ReachStepNonZero(i, i-1));
            limitSteps.add(new ArrayList<ReachStep>(result));
        }
    }

    public List<ReachStep> getSteps() {
        return limitSteps.get(count+1);
    }

    public int getCount() {
        return count;
    }
}
