package com.lexicalscope.bl.equiv;

import com.lexicalscope.bl.parser.BlParser.LocalReadContext;


public final class LocalReadExpression implements Expression {
    private final String var;

    public LocalReadExpression(final String var) {
        this.var = var;
    }

    public LocalReadExpression(final LocalReadContext expr) {
        this(expr.local.getText());
    }

    public String getVar() {
        return var;
    }

    @Override public String getType() {
        return "LocalRead";
    }

    @Override public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((var == null) ? 0 : var.hashCode());
        return result;
    }

    @Override public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocalReadExpression other = (LocalReadExpression) obj;
        if (var == null) {
            if (other.var != null) {
                return false;
            }
        } else if (!var.equals(other.var)) {
            return false;
        }
        return true;
    }

    @Override public String toString() {
        return "LocalReadExpression [var=" + var + "]";
    }

    public static Expression localOrConst(final String val) {
        return val.equals("null") ? new NullValueExpression() : new LocalReadExpression(val);
    }
}
