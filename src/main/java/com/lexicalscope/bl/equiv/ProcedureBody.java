package com.lexicalscope.bl.equiv;

import static com.google.common.primitives.Ints.checkedCast;
import static com.lexicalscope.Jc.$;
import static com.lexicalscope.bl.equiv.ExpressionFactory.expression;
import static com.lexicalscope.bl.equiv.StatementCollector.collectAtoms;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.lexicalscope.bl.parser.BlBaseListener;
import com.lexicalscope.bl.parser.BlParser.BlockContext;
import com.lexicalscope.bl.parser.BlParser.HeapReadContext;
import com.lexicalscope.bl.parser.BlParser.LocalReadContext;
import com.lexicalscope.bl.parser.BlParser.ModifiesSpecContext;
import com.lexicalscope.bl.parser.BlParser.SpecificationContext;

public class ProcedureBody {
    private static final class ReadExpressionCollector extends BlBaseListener {
        private final List<Expression> result;

        private ReadExpressionCollector(final List<Expression> result) {
            this.result = result;
        }

        @Override public void enterLocalRead(final LocalReadContext ctx) {
            result.add(expression(ctx));
        }

        @Override public void enterHeapRead(final HeapReadContext ctx) {
            result.add(expression(ctx));
        }
    }

    private final BlockContext block;
    private final List<String> params;
    private final SpecificationContext specification;

    public ProcedureBody(final List<String> params, final SpecificationContext specification, final BlockContext block) {
        this.params = params;
        this.specification = specification;
        this.block = block;
    }

    public Set<String> getLocals() {
        return $(collectAtoms(block)).
                 filterByType(LocalAssignment.class).
                 map(u -> u.getLhsVar()).
                 filterOutMembersOf(params).
                _$(LinkedHashSet::new);
    }

    public List<Statement> getStatements() {
        return new StatementCollector().collect(block);
    }

    public int getNewsCount() {
        return checkedCast($(getStatements()).filter(p -> p.isAlloc()).count());
    }

    public Modifies getModifies() {
        if(specification == null || specification.modifiesSpec() == null) {
            return null;
        }
        final ModifiesSpecContext modifiesSpec = specification.modifiesSpec();
        if(modifiesSpec.expressionSet().expr.isEmpty()) {
            return new EmptyModifies();
        }
        final List<Expression> result = new ArrayList<Expression>();
        new ParseTreeWalker().walk(new ReadExpressionCollector(result), modifiesSpec);
        return new ModifiesSet(result);
    }

    public List<Expression> getReads() {
        final List<Expression> result = new ArrayList<Expression>();
        if(specification != null && specification.readsSpec() != null) {
            new ParseTreeWalker().walk(new ReadExpressionCollector(result), specification.readsSpec());
        }
        return result;
    }

    @Override public String toString() {
        return String.format("ProcedureBody locals:%s", getLocals());
    }
}
