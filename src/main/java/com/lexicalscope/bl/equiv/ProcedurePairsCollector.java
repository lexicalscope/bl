package com.lexicalscope.bl.equiv;

import static com.lexicalscope.Jc.$;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.lexicalscope.bl.parser.BlBaseListener;
import com.lexicalscope.bl.parser.BlParser.MultiversionContext;
import com.lexicalscope.bl.parser.BlParser.ProcedureContext;
import com.lexicalscope.bl.parser.BlParser.VersionContext;

public class ProcedurePairsCollector {
    private interface CollectorState {
        CollectorState version(VersionContext ctx);

        void procedure(String name, ProcedureContext ctx);

        Map<String, ProcedurePair> pairs();
    }

    private static final class InitialState implements CollectorState {
        @Override public CollectorState version(final VersionContext ctx) {
            return new UnitOneState();
        }

        @Override public void procedure(final String name, final ProcedureContext ctx) {
            throw new IllegalStateException("procedure encountered outside version " + formatPosition(ctx));
        }

        @Override public Map<String, ProcedurePair> pairs() {
            throw new IllegalStateException("no versions encountered");
        }
    }

    private static final class UnitOneState implements CollectorState {
        private final Map<String, ProcedureContext> procedures = new LinkedHashMap<>();
        @Override public CollectorState version(final VersionContext ctx) {
            return new UnitTwoState(procedures);
        }

        @Override public void procedure(final String name, final ProcedureContext ctx) {
            if(procedures.containsKey(name)) {
                throw new IllegalStateException("too many declarations of " + name + " " + formatPosition(ctx));
            }
            procedures.put(name, ctx);
        }

        @Override public Map<String, ProcedurePair> pairs() {
            throw new IllegalStateException("not enough versions encountered");
        }
    }

    private static final class UnitTwoState implements CollectorState {
        private final Map<String, ProcedureContext> procedures;
        private final Map<String, ProcedurePair> pairs = new LinkedHashMap<>();

        public UnitTwoState(final Map<String, ProcedureContext> procedures) {
            this.procedures = procedures;
        }

        @Override public CollectorState version(final VersionContext ctx) {
            throw new IllegalStateException("too many versions " + formatPosition(ctx));
        }

        @Override public void procedure(final String name, final ProcedureContext ctx) {
            if(!procedures.containsKey(name)) {
                throw new IllegalStateException("no previous declaration of " + name + " " + formatPosition(ctx));
            } else if(pairs.containsKey(name)) {
                throw new IllegalStateException("too many declarations of " + name + " " + formatPosition(ctx));
            }
            pairs.put(name, new ProcedurePair(name, procedures.remove(name), ctx));
        }

        @Override public Map<String, ProcedurePair> pairs() {
            return pairs;
        }
    }

    private static class ProcedureCollector extends BlBaseListener {
        private CollectorState state = new InitialState();

        @Override public void enterVersion(final VersionContext ctx) {
            state = state.version(ctx);
        }

        @Override public void enterProcedure(final ProcedureContext ctx) {
            state.procedure(ctx.IDENTIFIER().getText(), ctx);
        }

        public List<ProcedurePair> pairs() {
            return $(state.pairs()).values()._$();
        }
    }

    public static ProcedurePairs procedurePairs(final MultiversionContext tree) {
        final ProcedureCollector procedureCollector = new ProcedureCollector();
        final ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(procedureCollector, tree);
        return new ProcedurePairs(procedureCollector.pairs());
    }

    static String formatPosition(final ParserRuleContext ctx) {
        return String.format("@%s:%s-%s:%s",
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                ctx.getStop().getLine(),
                ctx.getStop().getCharPositionInLine());
    }
}
