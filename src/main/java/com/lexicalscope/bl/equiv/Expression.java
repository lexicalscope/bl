package com.lexicalscope.bl.equiv;

public interface Expression {
    String getType();
    @Override int hashCode();
    @Override boolean equals(Object obj);
}
