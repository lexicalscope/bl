package com.lexicalscope.bl.equiv;

import static com.lexicalscope.bl.equiv.ExpressionFactory.expression;

import com.lexicalscope.bl.parser.BlParser.HeapReadContext;

public class HeapReadExpression implements Expression {
    private final Expression expression;

    public HeapReadExpression(final HeapReadContext expr) {
        this(expression(expr.expression()));
    }

    public HeapReadExpression(final String var) {
        this(new LocalReadExpression(var));
    }

    public HeapReadExpression(final Expression expression) {
        this.expression = expression;
    }

    @Override public String getType() {
        return "HeapRead";
    }

    public Expression getExpression() {
        return expression;
    }

    @Override public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expression == null) ? 0 : expression.hashCode());
        return result;
    }

    @Override public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HeapReadExpression other = (HeapReadExpression) obj;
        if (expression == null) {
            if (other.expression != null) {
                return false;
            }
        } else if (!expression.equals(other.expression)) {
            return false;
        }
        return true;
    }

    @Override public String toString() {
        return "HeapReadExpression [expression=" + expression + "]";
    }
}
