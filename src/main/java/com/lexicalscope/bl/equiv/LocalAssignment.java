package com.lexicalscope.bl.equiv;

public interface LocalAssignment {
    String getLhsVar();
}
