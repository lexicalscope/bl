preamble(reachability) ::= <<
type Ref;
type Heap;
type Data = [Ref]Ref;
type RefSet = [Ref]bool;
const unique null:Ref;

function HeapCons(Data,RefSet,RefSet):Heap;
function Data(Heap):Data;
function Allocated(Heap):RefSet;
function Roots(Heap):RefSet;

axiom (forall d:Data, al:RefSet, rts:RefSet :: {HeapCons(d,al,rts)} Data(HeapCons(d,al,rts))==d);
axiom (forall d:Data, al:RefSet, rts:RefSet :: {HeapCons(d,al,rts)} Allocated(HeapCons(d,al,rts))==al);
axiom (forall d:Data, al:RefSet, rts:RefSet :: {HeapCons(d,al,rts)} Roots(HeapCons(d,al,rts))==rts);

function {:inline true} WriteField(h:Heap, a:Ref, v:Ref) : Heap {HeapCons(Data(h)[a:=v], Allocated(h), Roots(h))}
function {:inline true} ReadField(h:Heap, a:Ref) : Ref {Data(h)[a]}
function {:inline true} Edge(h:Heap,a1:Ref,a2:Ref) : bool {ReadField(h,a1)==a2}
function {:inline true} Allocate(h:Heap, a:Ref) : Heap {HeapCons(Data(h), Allocated(h)[a:=true], Roots(h))}
function {:inline true} IsAllocated(h:Heap, a:Ref) : bool {Allocated(h)[a]}
function {:inline true} IsRoot(h:Heap, a:Ref) : bool {Roots(h)[a]}

///////////////////////////////////////////////////////////////////
// All the heaps created by BL programs have certain properties
function $GoodHeap(h:Heap) : bool;
axiom (forall h:Heap,a:Ref :: $GoodHeap(h) ==> !IsAllocated(h,a) ==> Edge(h,a,null));  
axiom (forall h:Heap :: $GoodHeap(h) ==> IsAllocated(h,null));
axiom (forall h:Heap :: $GoodHeap(h) ==> Edge(h,null,null));
axiom (forall h:Heap,a1,a2:Ref :: $GoodHeap(h) ==> IsAllocated(h,a1) && Edge(h,a1,a2) ==> IsAllocated(h,a2));
axiom (forall h:Heap,a:Ref :: {$ModOne(h,a)} $GoodHeap(h) ==> IsRoot(h,a) ==> IsAllocated(h,a));

///////////////////////////////////////////////////////////////////
// Used for performance, we only want to instantiate some
// quantifiers with objects explicitly allocated by the 
// body of a procedure
function $NewOne(h:Heap,a:Ref) : bool;
function $ModOne(h:Heap,a:Ref) : bool;

///////////////////////////////////////////////////////////////////
// HeapSuccessor
function $HeapSucc(h1,h2:Heap) : bool;
axiom (forall h:Heap,a,v:Ref :: {$GoodHeap(WriteField(h,a,v))} $GoodHeap(WriteField(h,a,v)) ==> $HeapSucc(h,WriteField(h,a,v)));
axiom (forall h:Heap,a:Ref :: {$GoodHeap(Allocate(h,a))} $GoodHeap(Allocate(h,a)) ==> $HeapSucc(h,Allocate(h,a)));
axiom (forall h1,h2,h3:Heap :: {$HeapSucc(h1,h2), $HeapSucc(h2,h3)} $HeapSucc(h1,h2) && $HeapSucc(h2,h3) ==> $HeapSucc(h1,h3));
axiom (forall h1,h2:Heap,a:Ref :: {$HeapSucc(h1,h2),$ModOne(h1,a)} $HeapSucc(h1,h2) && $ModOne(h1,a) ==> $ModOne(h2,a));
axiom (forall h1,h2:Heap,a:Ref :: {$HeapSucc(h1,h2),$NewOne(h1,a)} $HeapSucc(h1,h2) && $NewOne(h1,a) ==> $NewOne(h2,a));

///////////////////////////////////////////////////////////////////
// for now we do not allow free, but we only need to avoid
// address 1) memory reuse 2) read/write of freed memory
// which we could do by tracking freed memory and checking
// before each memory access
function NoFrees(h1,h2:Heap) : bool
{ (forall a:Ref :: IsAllocated(h1,a) ==> IsAllocated(h2,a))}

///////////////////////////////////////////////////////////////////
// Extensionality for Heaps
function Heap#Equal(Heap, Heap): bool;
axiom (forall h1,h2: Heap :: 
 Heap#Equal(h1,h2) <==> (forall a1,a2: Ref :: 
  {$ModOne(h1,a1),IsAllocated(h1,a2)} 
  {$ModOne(h2,a1),IsAllocated(h2,a2)} 
  (Edge(h1,a1,a2) == Edge(h2,a1,a2))) && 
  (forall a:Ref :: {$NewOne(h1,a)} {$NewOne(h2,a)} IsAllocated(h1,a)==IsAllocated(h2,a)));

axiom (forall h1,h2: Heap :: Heap#Equal(h1,h2) ==> h1 == h2);

///////////////////////////////////////////////////////////////////
// Roots are all allocated
function Roots#Equal(Heap, Heap): bool;
axiom (forall h1,h2:Heap :: Roots#Equal(h1,h2) <==> (forall a:Ref :: IsRoot(h1,a)<==>IsRoot(h2,a)));

///////////////////////////////////////////////////////////////////
// Same Modifications for Heaps
function Heap#SameMods(Heap, Heap, Heap, Heap): bool;
axiom (forall h1,h2,h3,h4:Heap ::
	Heap#SameMods(h1,h3,h2,h4) <==> 
	(forall a: Ref :: {$ModOne(h1,a)} {$NewOne(h1,a)} {IsAllocated(h1,a)} {IsAllocated(h2,a)} {IsAllocated(h3,a)} {IsAllocated(h4,a)} 
    	(ReadField(h2,a)!=ReadField(h4,a) ==> ReadField(h3,a)==ReadField(h4,a)) &&
    	(ReadField(h1,a)!=ReadField(h3,a) ==> ReadField(h3,a)==ReadField(h4,a)) &&
    	(!IsAllocated(h1,a) && IsAllocated(h3,a) <==> !IsAllocated(h2,a) && IsAllocated(h4,a)) &&
    	(IsAllocated(h1,a) && !IsAllocated(h3,a) <==> IsAllocated(h2,a) && !IsAllocated(h4,a)) 
));

///////////////////////////////////////////////////////////////////
// Restrictions on legal Isomorphisms
//
// An isomorphism must be consistent with null and
// the existing set of roots.
//
function Iso#MappingPreservesRoots(h1,h2:Heap, a1,a2:Ref) : bool
{
   (a1==null<==>a2==null) && // not allowed to permute null
   (IsRoot(h1,a1) == IsRoot(h2,a2)) && // only roots map to roots
   (IsRoot(h1,a1) ==> a1==a2) // roots mapped by identity
}

///////////////////////////////////////////////////////////////////
// Bounded Isomorphism and Reachability
///////////////////////////////////////////////////////////////////
// The number of steps refers to the number of maximum number of
// non-root objects which may be permuted to find an isomorphism.
//
// Zero steps means that the object under consideration points to
// the object we are trying to reach.
///////////////////////////////////////////////////////////////////
%reachability.steps:Reach();separator="\n"%
 
var h1:Heap where $GoodHeap(h1);
var h2:Heap where $GoodHeap(h2);

>> 

Reach(reach) ::= <<
///////////////////////////////////////////////////////////////////
// Reach from ar to a in a maximum of %reach.step% steps
//   
function Reach%reach.step%(h1:Heap,ar,a:Ref) : bool 
{ 
	Edge(h1,ar,a)
	%if(!reach.zero)%
		|| (!Edge(h1,ar,null) && Reach%reach.next%(h1,ReadField(h1,ar), a)) 
	%endif% 
}

///////////////////////////////////////////////////////////////////
// Reach a1 from ar1 by the same path as a2 from ar2 in a maximum
// of %reach.step% steps
// 
function ReachSame%reach.step%(h1,h2:Heap,ar1,ar2,a1,a2:Ref) : bool
{
    (Edge(h1,ar1,a1) && Edge(h2,ar2,a2)) 
	%if(!reach.zero)%
		|| (!Edge(h1,ar1,null) && // just an optimisation 
		    !Edge(h2,ar2,null) && // just an optimisation 
		    ReachSame%reach.next%(h1,h2,ReadField(h1,ar1),ReadField(h2,ar2),a1,a2))
	%endif%
}

///////////////////////////////////////////////////////////////////
// Isomorphism for heaps from roots to depth n
//
function Heap#Iso%reach.step%(h1,h2:Heap):bool
{
	(forall ar1,a1,a2:Ref :: 
		{IsAllocated(h1,ar1),$NewOne(h1,a1),$NewOne(h2,a2)}
		Heap#Iso#ReachSameFromRoot%reach.step%(h1,h2,ar1,a1,a2) ==>
		    Iso#MappingPreservesRoots(h1,h2,a1,a2) && 
			Heap#Iso#ConsistentAliasing%reach.step%(h1,h2,a1,a2)
	)
}

function Heap#Iso#ConsistentAliasing%reach.step%(h1,h2:Heap, a1,a2:Ref):bool
{
   (forall ar2,a3,a4:Ref :: {IsAllocated(h1,ar2),$NewOne(h1,a3),$NewOne(h2,a4)} 
		Heap#Iso#ReachSameFromRoot%reach.step%(h1,h2,ar2,a3,a4) ==> (a1==a3<==>a2==a4))
}

function Heap#Iso#ReachSameFromRoot%reach.step%(h1,h2:Heap, ar,a1,a2:Ref):bool
{
	IsRoot(h1,ar) && ReachSame%reach.step%(h1,h2,ar,ar,a1,a2)
}

			
>>
